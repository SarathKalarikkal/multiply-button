import React, { useState } from 'react';
import "./App.css";

function App() {
  const [count, setCount] = useState(1);

  return (
    <div className='counterBox'>
      <h1>{count}</h1>
      <button onClick={() => setCount(count * 5)}>
        Multiply
        <span className="tooltip">Click to multiply by 5</span>
      </button>
      <button className='reset' onClick={() => setCount(1)}>
        Reset
        <span className="tooltip">Click to reset</span>
      </button>
    </div>
  );
}

export default App;
